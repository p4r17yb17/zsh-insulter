# zsh-insulter
Randomly insults the user when typing wrong command.
This is heavily based on [hkbakke's](https://github.com/hkbakke/bash-insulter) work and copied from [matmutant's](https://github.com/matmutant/zsh-insulter) work.

Change insults as needed :)

![zshInsults](https://gitlab.com/p4r17yb17/zsh-insulter/-/raw/master/zshInsults.png)

# Installation

## Manual
    git clone https://gitlab.com/p4r17yb17/zsh-insulter.git zsh-insulter
    grab the zsh.command-not-found and paste it where-ever you want. (e.g. ~/.zshInsulter/)

Then source the file by adding the following to the `.zshrc`:
```
if [ -f ~/.zshInsulter/zsh.command-not-found ]; then
    . ~/.zshInsulter/zsh.command-not-found
fi
```
Start a new zsh shell and type some invalid commands for the effects to be visible.

## Arch makepkg
```
git clone https://gitlab.com/p4r17yb17/zsh-insulter.git
cd zsh-insulter
makepkg -si
```
